//
//  CurrencyVC.h
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrencyVC : UITableViewController<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
    NSMutableArray *_currencyArray;
}
@end
