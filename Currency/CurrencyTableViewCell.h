//
//  CurrencyTableViewCell.h
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrencyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *txtname;
@property (weak, nonatomic) IBOutlet UILabel *txtbuy;
@property (weak, nonatomic) IBOutlet UILabel *txtsell;
@property (weak, nonatomic) IBOutlet UILabel *txtrate;

@end
