//
//  AppDelegate.h
//  Currency
//
//  Created by Mert Saygı on 08/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

