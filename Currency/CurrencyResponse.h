//
//  CurrencyResponse.h
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyResponse : NSObject

@property (retain, nonatomic) NSString *type;
@property (retain, nonatomic) NSString *buy;
@property (retain, nonatomic) NSString *sell;
@property (retain, nonatomic) NSString *diff;

@end
