//
//  NewsTableViewCell.h
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *txtheader;
@property (weak, nonatomic) IBOutlet UIImageView *imgnews;

@end
