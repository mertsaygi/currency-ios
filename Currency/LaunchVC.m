//
//  LaunchVC.m
//  Currency
//
//  Created by Mert Saygı on 14/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import "LaunchVC.h"
#import "NewsTableViewCell.h"
#import "NewsResponse.h"
#import "CurrencyVC.h"
#import "NewsDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface LaunchVC ()

@end

@implementation LaunchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Geri" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self setTitle: @"Currency"];
    
    UIBarButtonItem *btnCurrencies = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(openCurrencies:)];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnCurrencies, nil]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://45.55.41.170:8080/news/"]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

-(void)openCurrencies:(id)sender
{
    CurrencyVC *currency = [self.storyboard instantiateViewControllerWithIdentifier:@"currencyVC"];
    [self.navigationController pushViewController:currency animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"%@",response);
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self dataPreProcess:_responseData];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

}

-(void)dataPreProcess:(NSMutableData*)data{
    NSLog(@"String sent from server %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:_responseData options:NSJSONReadingMutableLeaves error:nil];
    _newsArray = [[NSMutableArray alloc] init];
    NSArray *results = dic;
    for (NSDictionary *result in results) {
        NewsResponse *news = [NewsResponse new];
        news.image = [result objectForKey:@"image"];
        news.url = [result objectForKey:@"url"];
        news.title=[result objectForKey:@"title"];
        [_newsArray addObject:news];
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _newsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"NewsCell";
    
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[NewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    NewsResponse *news = [_newsArray objectAtIndex:indexPath.row];
    
    if(![news.image isEqual: [NSNull null]]){
        [cell.imgnews sd_setImageWithURL:[NSURL URLWithString:news.image]];
    }
    
    if(![news.title isEqual: [NSNull null]]){
        cell.txtheader.text = news.title;
        cell.txtheader.lineBreakMode = UILineBreakModeWordWrap;
        cell.txtheader.numberOfLines = 0;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsResponse *news = [_newsArray objectAtIndex:indexPath.row];
    NewsDetailViewController *newsDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"newsDetail"];
    newsDetail.url = news.url;
    [self.navigationController pushViewController:newsDetail animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
