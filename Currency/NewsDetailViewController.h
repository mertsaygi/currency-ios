//
//  NewsDetailViewController.h
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailViewController : UIViewController

@property (retain, nonatomic) NSString *url;
@property (weak, nonatomic) IBOutlet UIWebView *newsDetailWebView;

@end
