//
//  CurrencyVC.m
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import "CurrencyVC.h"
#import "CurrencyTableViewCell.h"
#import "CurrencyResponse.h"

@interface CurrencyVC ()

@end

@implementation CurrencyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://45.55.41.170:8080/live/"]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"%@",response);
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self dataPreProcess:_responseData];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

-(void)dataPreProcess:(NSMutableData*)data{
    NSLog(@"String sent from server %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:_responseData options:NSJSONReadingMutableLeaves error:nil];
    _currencyArray = [[NSMutableArray alloc] init];
    NSArray *results = dic;
    [self addHeader];
    for (NSDictionary *result in results) {
        CurrencyResponse *currency = [CurrencyResponse new];
        currency.type = [result objectForKey:@"type"];
        currency.buy = [result objectForKey:@"buy"];
        currency.sell=[result objectForKey:@"sell"];
        currency.diff=[result objectForKey:@"diff"];
        [_currencyArray addObject:currency];
    }
    [self.tableView reloadData];
}

-(void)addHeader{
    CurrencyResponse *currency = [CurrencyResponse new];
    currency.type = @"-";
    currency.buy = @"Alış";
    currency.sell=@"Satış";
    currency.diff=@"";
    [_currencyArray addObject:currency];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _currencyArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CurrencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"currencyRow" forIndexPath:indexPath];
    
    CurrencyResponse *currency = [_currencyArray objectAtIndex:indexPath.row];
    
    NSArray *symbol = [currency.type componentsSeparatedByString:@"-"];
    cell.txtname.text = symbol[1];
    cell.txtbuy.text = currency.buy;
    cell.txtsell.text = currency.sell;
    cell.txtrate.text = currency.diff;
    
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
