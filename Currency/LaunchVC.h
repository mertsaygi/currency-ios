//
//  LaunchVC.h
//  Currency
//
//  Created by Mert Saygı on 14/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchVC : UITableViewController<NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
    NSMutableArray *_newsArray;
}
@end
