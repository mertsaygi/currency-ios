//
//  NewsResponse.h
//  Currency
//
//  Created by Mert Saygı on 15/12/15.
//  Copyright © 2015 Mert Saygı. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsResponse : NSObject

@property (retain, nonatomic) NSString *url;
@property (retain, nonatomic) NSString *image;
@property (retain, nonatomic) NSString *title;

@end
